---
Spike Report
---

Core Spike 3
============

Introduction
------------

Now that the user has some working knowledge of basic Unreal Engine Blueprints
they can put it to use by creating some basic classes to create a very
simple game.

Bitbucket Link: <https://bitbucket.org/bSalmon842/2018_spike3>

Goals
-----

1.	A small Unreal Engine project, built from scratch (can use content
	from any project from the Learn Tab in the Epic Games Launcher).
	
	a.	Any small game which you like, manage the scope yourself.
	
	b.	I would recommend finding a classic Arcade game that you want to
	copy.
	
2.	The project must include the following custom classes (i.e. child classes):

	a. Game Mode
	
		i.	Sets the game to use the Pawn, Controller and GameState below
		
	b.	A Pawn or Character
	
		i.	Game Logic for any playable/controllable Actors goes here
		
	c.	A Player Controller
		
		i.	Control logic goes here (i.e. how to convert Input for the Pawn to use)
		
		ii.	It must support at LEAST: Mouse and Keyboard

		iii. Should also support one of:
			
			1.	Game Pad
			
			2.	Virtual Reality Controllers
			
	d.	A GameState
	
		i.	It should track a Score of some kind (or Timer, or similar)

Personnel
---------

  ------------------------- ------------------
  Primary -- Brock Salmon   Secondary -- N/A
  ------------------------- ------------------

Technologies, Tools, and Resources used
---------------------------------------

-	<https://docs.unrealengine.com/en-us/Gameplay/Framework>

-	<https://docs.unrealengine.com/en-us/Engine/Blueprints>

Tasks undertaken
----------------

1.  Start by creating the Blueprints deriving from GameModeBase, Pawn,
	PlayerController, & GameStateBase.

2.  In the GameMode Blueprint set the relevant Blueprints to the Default
	Player Controller, Player Pawn, and GameState

3.	Create an Actor Blueprint for the collectables in the level.	
	
4.  Create a Small Map for the 'Sphere Hunt' Game where the objective is
	to collect all the spheres in the level. Some Sphere Actors and the
	Player Pawn is placed in the level.

5.  Setup the Input in Project Settings-\>Inputs for the game.

6.  Setup the Player Controller to use the setup Input Axis to move the Player Pawn.

7.  In the PlayerPawn Blueprint setup a Camera and Camera Boom so that the camera
	points down over the Player.

8.  In the PlayerPawn Blueprint detect when the Player collides with a Sphere and delete that sphere.

9.  Test that the level can be played as intended, albeit without any objective tracking or end.

10.  In the GameState Blueprint on each tick get the number of Spheres in the level, if there are
	still spheres in the level then show the player a Timer informing them of how long they have
	been playing, and when they collect the last sphere, stop the timer and show the player how
	long the level took them

What we found out
-----------------

In this Spike we learned how to create a small level with the use of Unreal Engine's Blueprint
System, including how to properly set out handling of different areas of the game into appropriate
Blueprint classes. We learned how to derive Blueprints to create other blueprints, taking advantage
of Unreal Engine and C++'s Object Oriented Design. We derived from several different classes that
are integral to how the game functions and by doing so, learned some of the best practices for how
to split up functionality in the game to each section. UE4 by default sets up these classes for the
developer, however understanding how to create your own is vital when using a commercial engine so
that you understand what is happening behind the scenes, and as such when you make changes to these
classes or decide to create your own you understand how the changes will affect the gameplay.
